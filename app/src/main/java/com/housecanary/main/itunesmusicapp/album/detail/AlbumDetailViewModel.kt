package com.housecanary.main.itunesmusicapp.album.detail

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.housecanary.main.itunesmusicapp.adapter.DiffItem
import com.housecanary.main.itunesmusicapp.network.models.AlbumModel

class AlbumDetailViewModel(val model: AlbumModel) : BaseObservable(), DiffItem {

    @get:Bindable
    val name: String? by lazy {
        model.collectionName
    }

    @get:Bindable
    val type: String? by lazy {
        model.collectionType
    }

    @get:Bindable
    val price: String? by lazy {
        "$" + model.collectionPrice
    }

    @get:Bindable
    val linkUrl: String? by lazy {
        model.collectionViewUrl
    }

    override fun areItemsTheSame(item: DiffItem): Boolean {
        return if (item is AlbumDetailViewModel) {
            item.model.collectionId == model.collectionId
        } else {
            false
        }
    }

    override fun areContentsTheSame(item: DiffItem): Boolean {
        val viewModel = item as AlbumDetailViewModel
        return viewModel.model.collectionName == model.collectionName &&
                viewModel.model.collectionType == model.collectionType &&
                viewModel.model.collectionPrice == model.collectionPrice &&
                viewModel.model.collectionViewUrl == model.collectionViewUrl
    }

}