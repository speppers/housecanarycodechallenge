package com.housecanary.main.itunesmusicapp.album

import com.housecanary.main.itunesmusicapp.adapter.DiffItem
import com.housecanary.main.itunesmusicapp.adapter.sectionModel.SectionModel
import com.housecanary.main.itunesmusicapp.album.detail.AlbumDetailViewModel
import com.housecanary.main.itunesmusicapp.album.song.SongViewModel
import com.housecanary.main.itunesmusicapp.network.models.AlbumModel

class SongSectionModel(model: AlbumModel) : SectionModel() {

    init {
        headerModel = AlbumDetailViewModel(model)
    }

    override fun isForSection(item: DiffItem): Boolean {
        return item is SongViewModel
    }

}