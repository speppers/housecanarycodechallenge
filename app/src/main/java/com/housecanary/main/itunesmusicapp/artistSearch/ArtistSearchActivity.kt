package com.housecanary.main.itunesmusicapp.artistSearch

import android.app.SearchManager
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.appcompat.widget.Toolbar
import android.view.Menu
import android.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import com.housecanary.main.itunesmusicapp.R
import com.housecanary.main.itunesmusicapp.activity.BaseActivity
import com.housecanary.main.itunesmusicapp.databinding.ActivityArtistSearchBinding

class ArtistSearchActivity : BaseActivity() {

    lateinit var searchView: SearchView
    lateinit var viewModel: ArtistSearchViewModel
    lateinit var binding: ActivityArtistSearchBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView<ViewDataBinding>(this, R.layout.activity_artist_search) as ActivityArtistSearchBinding
        viewModel = ViewModelProvider(this).get(ArtistSearchViewModel::class.java)
        binding.viewModel = viewModel
        setupRecyclerView()
        setSupportActionBar(binding.toolbar as Toolbar)
    }

    private fun setupRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = viewModel.adapter
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)

        val searchItem = menu.findItem(R.id.search)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager

        searchView = searchItem.actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                searchView.clearFocus()
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                if (newText.isEmpty()) {
                    viewModel.clearItems()
                } else {
                    viewModel.searchViewModel.push(newText)
                }

                return true
            }
        })

        return super.onCreateOptionsMenu(menu)
    }
}
