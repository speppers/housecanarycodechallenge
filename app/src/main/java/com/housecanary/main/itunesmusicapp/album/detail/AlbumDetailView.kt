package com.housecanary.main.itunesmusicapp.album.detail

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import com.housecanary.main.itunesmusicapp.adapter.BindableView
import com.housecanary.main.itunesmusicapp.databinding.ViewAlbumDetailBinding

class AlbumDetailView(context: Context) : FrameLayout(context), BindableView<AlbumDetailViewModel> {

    private val binding = ViewAlbumDetailBinding.inflate(LayoutInflater.from(context), this, true)

    init {
        layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun bind(viewModel: AlbumDetailViewModel) {
        binding.viewModel = viewModel
    }

}