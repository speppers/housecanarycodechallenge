package com.housecanary.main.itunesmusicapp.dagger

import com.housecanary.main.itunesmusicapp.dagger.modules.RepositoryModule
import com.housecanary.main.itunesmusicapp.dagger.modules.RetrofitModule

object Injector {

    val component: ApplicationComponent by lazy {
        DaggerApplicationComponent
                .builder()
                .repositoryModule(RepositoryModule())
                .retrofitModule(RetrofitModule())
                .build()
    }
}