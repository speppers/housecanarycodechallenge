package com.housecanary.main.itunesmusicapp.network.models

class AlbumResponse(
        val resultCount: Int,
        val results: List<AlbumModel>
)