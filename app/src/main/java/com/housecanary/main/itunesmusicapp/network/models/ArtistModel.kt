package com.housecanary.main.itunesmusicapp.network.models

import org.parceler.Parcel

@Parcel
class ArtistModel {
    var wrapperType: String? = null
    var artistType: String? = null
    var artistName: String? = null
    var artistLinkUrl: String? = null
    var artistId: Int = 0
    var amgArtistId: Int? = 0
    var primaryGenreName: String? = null
    var primaryGenreId: Int? = 0
}