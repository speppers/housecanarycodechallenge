package com.housecanary.main.itunesmusicapp.artistProfile

import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import com.housecanary.main.itunesmusicapp.Constants
import com.housecanary.main.itunesmusicapp.R
import com.housecanary.main.itunesmusicapp.activity.BaseActivity
import com.housecanary.main.itunesmusicapp.databinding.ActivityArtistProfileBinding
import com.housecanary.main.itunesmusicapp.network.models.ArtistModel
import org.parceler.Parcels

class ArtistProfileActivity : BaseActivity() {

    lateinit var binding : ActivityArtistProfileBinding
    lateinit var viewModel: ArtistProfileViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView<ViewDataBinding>(this, R.layout.activity_artist_profile) as ActivityArtistProfileBinding
        viewModel = ViewModelProvider(this).get(ArtistProfileViewModel::class.java)
        getIntentExtras()
        setupToolbar()
        setupRecyclerview()
    }

    private fun setupToolbar() {
        setSupportActionBar(binding.toolbar as Toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun setupRecyclerview() {
        val spanSize = 2
        val layoutManager = GridLayoutManager(this, spanSize)

        layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (position == 0) {
                    spanSize
                } else {
                    1
                }
            }
        }

        binding.recyclerView.layoutManager = layoutManager
        binding.recyclerView.adapter = viewModel.adapter
    }

    private fun getIntentExtras() {
        val extras = intent.extras
        if (extras != null) {
            if (extras.containsKey(Constants.artist)) {
                viewModel.artist = Parcels.unwrap<ArtistModel>(extras.getParcelable(Constants.artist))
            }
        }
    }
}