package com.housecanary.main.itunesmusicapp.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate

abstract class AbstractAdapterDelegate<TModel : TBase, TBase, TView : View> : AdapterDelegate<MutableList<TBase>>() {

    override fun isForViewType(items: MutableList<TBase>, position: Int): Boolean {
        return isForViewType(items[position], items, position)
    }

    @Suppress("UNCHECKED_CAST")
    override fun onBindViewHolder(items: MutableList<TBase>,
                                  position: Int,
                                  holder: RecyclerView.ViewHolder,
                                  payloads: MutableList<Any>) {
        onBindView(items[position] as TModel, holder.itemView as TView)
    }

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return BaseViewHolder((onCreateView(parent)))
    }

    protected abstract fun isForViewType(item: TBase, items: List<TBase>, position: Int): Boolean

    protected abstract fun onCreateView(parent: ViewGroup): TView

    protected abstract fun onBindView(item: TModel, view: TView)

}