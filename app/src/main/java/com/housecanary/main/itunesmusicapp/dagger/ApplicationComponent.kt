package com.housecanary.main.itunesmusicapp.dagger

import com.housecanary.main.itunesmusicapp.album.AlbumDisplayViewModel
import com.housecanary.main.itunesmusicapp.artistProfile.ArtistProfileViewModel
import com.housecanary.main.itunesmusicapp.artistSearch.ArtistSearchViewModel
import com.housecanary.main.itunesmusicapp.dagger.modules.RepositoryModule
import com.housecanary.main.itunesmusicapp.dagger.modules.RetrofitModule
import com.housecanary.main.itunesmusicapp.dagger.modules.ServiceModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [RepositoryModule::class, RetrofitModule::class, ServiceModule::class])
interface ApplicationComponent {

    fun inject(viewModel: ArtistSearchViewModel)
    fun inject(viewModel: ArtistProfileViewModel)
    fun inject(viewModel: AlbumDisplayViewModel)

}