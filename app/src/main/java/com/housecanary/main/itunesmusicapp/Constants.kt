package com.housecanary.main.itunesmusicapp

object Constants {

    const val id = "id"
    const val artist = "artist"
    const val term = "term"
    const val entity = "entity"
    const val musicArtist = "musicArtist"
    const val album = "album"
    const val song = "song"

}