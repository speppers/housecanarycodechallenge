package com.housecanary.main.itunesmusicapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class ViewModelBase(application: Application) : AndroidViewModel(application), Observable {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private val callbacks = PropertyChangeRegistry()

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        callbacks.add(callback)
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        callbacks.remove(callback)
    }

    protected fun notifyChange() {
        callbacks.notifyCallbacks(this, 0, null)
    }

    protected fun notifyPropertyChanged(fieldId: Int) {
        callbacks.notifyCallbacks(this, fieldId, null)
    }

    protected fun addDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}