package com.housecanary.main.itunesmusicapp.network.repositories

import com.housecanary.main.itunesmusicapp.Constants
import com.housecanary.main.itunesmusicapp.extensions.async
import com.housecanary.main.itunesmusicapp.network.ItunesService
import com.housecanary.main.itunesmusicapp.network.models.SongModel
import io.reactivex.Single

class SongRepository(private val itunesService: ItunesService) {

    fun getSongsForAlbum(albumId: Int) : Single<List<SongModel>> {
        val params = HashMap<String, String>()
        params[Constants.id] = albumId.toString()
        params[Constants.entity] = Constants.song

        return itunesService
                .lookupSongs(params)
                .map { response ->
                    response.results.filter { it.wrapperType == "track" }
                }
                .async()
    }

}