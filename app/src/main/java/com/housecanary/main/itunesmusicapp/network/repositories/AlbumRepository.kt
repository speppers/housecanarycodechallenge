package com.housecanary.main.itunesmusicapp.network.repositories

import com.housecanary.main.itunesmusicapp.Constants
import com.housecanary.main.itunesmusicapp.extensions.async
import com.housecanary.main.itunesmusicapp.network.ItunesService
import com.housecanary.main.itunesmusicapp.network.models.AlbumModel
import io.reactivex.Single

class AlbumRepository(private val itunesService: ItunesService) {

    fun getAlbumsForArtist(artistId: Int) : Single<List<AlbumModel>> {
        val params = HashMap<String, String>()
        params[Constants.id] = artistId.toString()
        params[Constants.entity] = Constants.album

        return itunesService
                .lookupAlbums(params)
                .map { response ->
                    response.results.filter { it.collectionId > 0 }
                }
                .async()
    }

}