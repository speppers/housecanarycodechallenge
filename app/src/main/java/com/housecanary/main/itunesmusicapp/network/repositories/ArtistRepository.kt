package com.housecanary.main.itunesmusicapp.network.repositories

import com.housecanary.main.itunesmusicapp.Constants.entity
import com.housecanary.main.itunesmusicapp.Constants.musicArtist
import com.housecanary.main.itunesmusicapp.Constants.term
import com.housecanary.main.itunesmusicapp.extensions.async
import com.housecanary.main.itunesmusicapp.network.ItunesService
import com.housecanary.main.itunesmusicapp.network.models.ArtistModel
import io.reactivex.Single

class ArtistRepository(private val itunesService: ItunesService) {

    fun get(searchText: String) : Single<List<ArtistModel>> {
        val params = HashMap<String, String>()
        params[term] = searchText
        params[entity] = musicArtist

        return itunesService
                .searchArtists(params)
                .map { it.results }
                .async()
    }

}