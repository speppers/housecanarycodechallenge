package com.housecanary.main.itunesmusicapp.dagger.modules

import com.housecanary.main.itunesmusicapp.network.repositories.ArtistRepository
import com.housecanary.main.itunesmusicapp.network.ItunesService
import com.housecanary.main.itunesmusicapp.network.repositories.AlbumRepository
import com.housecanary.main.itunesmusicapp.network.repositories.SongRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideArtistRepository(itunesService: ItunesService) : ArtistRepository {
        return ArtistRepository(itunesService)
    }

    @Provides
    @Singleton
    fun provideAlbumRepository(itunesService: ItunesService) : AlbumRepository {
        return AlbumRepository(itunesService)
    }

    @Provides
    @Singleton
    fun provideSongRepository(itunesService: ItunesService) : SongRepository {
        return SongRepository(itunesService)
    }

}