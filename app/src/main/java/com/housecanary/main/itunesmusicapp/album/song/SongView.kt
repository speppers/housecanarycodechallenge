package com.housecanary.main.itunesmusicapp.album.song

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import com.housecanary.main.itunesmusicapp.adapter.BindableView
import com.housecanary.main.itunesmusicapp.databinding.ViewSongBinding

class SongView(context: Context) : FrameLayout(context), BindableView<SongViewModel> {

    private val binding = ViewSongBinding.inflate(LayoutInflater.from(context), this, true)

    init {
        layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun bind(viewModel: SongViewModel) {
        binding.viewModel = viewModel
    }

}