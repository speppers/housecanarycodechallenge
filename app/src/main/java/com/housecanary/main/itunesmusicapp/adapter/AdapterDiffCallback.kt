package com.housecanary.main.itunesmusicapp.adapter

import androidx.recyclerview.widget.DiffUtil

class AdapterDiffCallback(payload: ItemChangePayload) : DiffUtil.Callback() {

    private val oldList: List<DiffItem> = payload.oldItems
    private val newList: List<DiffItem> = payload.newItems

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]

        return oldItem.areItemsTheSame(newItem)
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]

        return oldItem.areContentsTheSame(newItem)
    }
}