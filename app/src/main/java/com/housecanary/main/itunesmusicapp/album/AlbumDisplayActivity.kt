package com.housecanary.main.itunesmusicapp.album

import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import com.housecanary.main.itunesmusicapp.Constants
import com.housecanary.main.itunesmusicapp.R
import com.housecanary.main.itunesmusicapp.activity.BaseActivity
import com.housecanary.main.itunesmusicapp.databinding.ActivityAlbumsBinding
import com.housecanary.main.itunesmusicapp.network.models.AlbumModel
import org.parceler.Parcels

class AlbumDisplayActivity : BaseActivity() {

    lateinit var binding: ActivityAlbumsBinding
    lateinit var viewModel: AlbumDisplayViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView<ViewDataBinding>(this, R.layout.activity_albums) as ActivityAlbumsBinding
        viewModel = ViewModelProvider(this).get(AlbumDisplayViewModel::class.java)
        getIntentExtras()
        setupToolbar()
        setupRecyclerView()
    }

    private fun setupToolbar() {
        val toolbar = binding.toolbar as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun setupRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = viewModel.adapter
    }

    private fun getIntentExtras() {
        val extras = intent.extras
        if (extras != null) {
            if (extras.containsKey(Constants.album)) {
                viewModel.album = Parcels.unwrap<AlbumModel>(extras.getParcelable(Constants.album))
            }
        }
    }

}