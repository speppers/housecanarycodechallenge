package com.housecanary.main.itunesmusicapp.dagger.modules

import com.housecanary.main.itunesmusicapp.network.ItunesService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class ServiceModule {

    @Provides
    internal fun getItunesService(retrofit: Retrofit): ItunesService {
        return retrofit.create(ItunesService::class.java)
    }

}