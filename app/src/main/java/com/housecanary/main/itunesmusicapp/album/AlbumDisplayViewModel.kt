package com.housecanary.main.itunesmusicapp.album

import android.app.Application
import com.housecanary.main.itunesmusicapp.adapter.AdapterDelegateFactory
import com.housecanary.main.itunesmusicapp.adapter.DiffAdapter
import com.housecanary.main.itunesmusicapp.album.detail.AlbumDetailView
import com.housecanary.main.itunesmusicapp.album.detail.AlbumDetailViewModel
import com.housecanary.main.itunesmusicapp.album.song.SongView
import com.housecanary.main.itunesmusicapp.album.song.SongViewModel
import com.housecanary.main.itunesmusicapp.artistProfile.ArtistProfileSectionModel
import com.housecanary.main.itunesmusicapp.artistProfile.album.AlbumViewModel
import com.housecanary.main.itunesmusicapp.dagger.Injector
import com.housecanary.main.itunesmusicapp.network.models.AlbumModel
import com.housecanary.main.itunesmusicapp.network.models.ArtistModel
import com.housecanary.main.itunesmusicapp.network.repositories.SongRepository
import com.housecanary.main.itunesmusicapp.viewModel.ViewModelBase
import java.util.*
import javax.inject.Inject

class AlbumDisplayViewModel(application: Application) : ViewModelBase(application) {

    @Inject
    lateinit var songRepository: SongRepository
    var adapter: DiffAdapter = DiffAdapter(listOf(
            AdapterDelegateFactory.create(AlbumDetailViewModel::class.java, AlbumDetailView::class.java),
            AdapterDelegateFactory.create(SongViewModel::class.java, SongView::class.java)))

    var album: AlbumModel? = null
        set(value) {
            field = value
            updateSongs()
        }

    init {
        Injector.component.inject(this)
    }

    private fun updateSongs() {
        album?.let { albumModel ->
            adapter.setSections(Arrays.asList(SongSectionModel(albumModel)))
            adapter.update(ArrayList()) //Display the header immediately

            addDisposable(songRepository.getSongsForAlbum(albumModel.collectionId).subscribe({ models ->
                adapter.update(models.map { SongViewModel(it) })
            }, Throwable::printStackTrace))
        }
    }

}