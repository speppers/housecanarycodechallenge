package com.housecanary.main.itunesmusicapp.artistSearch.artist

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.FrameLayout
import com.housecanary.main.itunesmusicapp.Constants
import com.housecanary.main.itunesmusicapp.adapter.BindableView
import com.housecanary.main.itunesmusicapp.artistProfile.ArtistProfileActivity
import com.housecanary.main.itunesmusicapp.databinding.ViewArtistBinding
import org.parceler.Parcels

class ArtistView(context: Context) : FrameLayout(context), BindableView<ArtistViewModel> {

    private val binding: ViewArtistBinding = ViewArtistBinding.inflate(LayoutInflater.from(context), this, true)
    private var viewModel: ArtistViewModel? = null

    init {
        layoutParams = FrameLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
        setOnClickListener {
            val intent = Intent(context, ArtistProfileActivity::class.java).apply {
                putExtra(Constants.artist, Parcels.wrap(viewModel?.model))
            }
            context.startActivity(intent)
        }
    }

    override fun bind(viewModel: ArtistViewModel) {
        this.viewModel = viewModel
        binding.viewModel = viewModel
    }

}