package com.housecanary.main.itunesmusicapp.artistSearch.artist

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.housecanary.main.itunesmusicapp.adapter.DiffItem
import com.housecanary.main.itunesmusicapp.network.models.ArtistModel

class ArtistViewModel(val model: ArtistModel) : BaseObservable(), DiffItem {

    @get:Bindable
    val name: String? by lazy {
        model.artistName
    }

    override fun areItemsTheSame(item: DiffItem): Boolean {
        return if (item is ArtistViewModel) {
            item.model.artistId == model.artistId
        } else {
            false
        }
    }

    override fun areContentsTheSame(item: DiffItem): Boolean {
        val viewModel = item as ArtistViewModel
        return viewModel.model.artistName == model.artistName
    }

}