package com.housecanary.main.itunesmusicapp.artistProfile

import com.housecanary.main.itunesmusicapp.adapter.DiffItem
import com.housecanary.main.itunesmusicapp.adapter.sectionModel.SectionModel
import com.housecanary.main.itunesmusicapp.artistProfile.album.AlbumViewModel
import com.housecanary.main.itunesmusicapp.artistProfile.profileHeader.ProfileHeaderViewModel
import com.housecanary.main.itunesmusicapp.network.models.ArtistModel

class ArtistProfileSectionModel(artistModel : ArtistModel) : SectionModel() {

    init {
        headerModel = ProfileHeaderViewModel(artistModel)
    }

    override fun isForSection(item: DiffItem): Boolean {
        return item is AlbumViewModel
    }

}