package com.housecanary.main.itunesmusicapp.network.models

class ArtistSearchResponse(
        val resultCount: Int,
        val results: List<ArtistModel>
)