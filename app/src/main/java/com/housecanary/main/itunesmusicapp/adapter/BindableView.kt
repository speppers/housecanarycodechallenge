package com.housecanary.main.itunesmusicapp.adapter

interface BindableView<T> {
    fun bind(viewModel: T)
}