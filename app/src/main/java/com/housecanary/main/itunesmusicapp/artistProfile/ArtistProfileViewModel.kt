package com.housecanary.main.itunesmusicapp.artistProfile

import android.app.Application
import com.housecanary.main.itunesmusicapp.adapter.AdapterDelegateFactory
import com.housecanary.main.itunesmusicapp.adapter.DiffAdapter
import com.housecanary.main.itunesmusicapp.artistProfile.album.AlbumView
import com.housecanary.main.itunesmusicapp.artistProfile.album.AlbumViewModel
import com.housecanary.main.itunesmusicapp.artistProfile.profileHeader.ProfileHeaderView
import com.housecanary.main.itunesmusicapp.artistProfile.profileHeader.ProfileHeaderViewModel
import com.housecanary.main.itunesmusicapp.dagger.Injector
import com.housecanary.main.itunesmusicapp.network.models.ArtistModel
import com.housecanary.main.itunesmusicapp.network.repositories.AlbumRepository
import com.housecanary.main.itunesmusicapp.viewModel.ViewModelBase
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class ArtistProfileViewModel(application: Application) : ViewModelBase(application) {

    @Inject
    lateinit var albumRepository: AlbumRepository
    var adapter: DiffAdapter = DiffAdapter(listOf(
            AdapterDelegateFactory.create(ProfileHeaderViewModel::class.java, ProfileHeaderView::class.java),
            AdapterDelegateFactory.create(AlbumViewModel::class.java, AlbumView::class.java)
    ))

    init {
        Injector.component.inject(this)
    }

    var artist: ArtistModel? = null
        set(value) {
            field = value
            updateAlbums()
        }

    private fun updateAlbums() {
        artist?.let { artistModel ->
            adapter.setSections(Arrays.asList(ArtistProfileSectionModel(artistModel)))
            adapter.update(ArrayList()) //Display the header immediately

            addDisposable(albumRepository.getAlbumsForArtist(artistModel.artistId).subscribe({ models ->
                adapter.update(models.map { AlbumViewModel(it) })
            }, Throwable::printStackTrace))
        }
    }

}