package com.housecanary.main.itunesmusicapp.network

import com.housecanary.main.itunesmusicapp.network.models.AlbumResponse
import com.housecanary.main.itunesmusicapp.network.models.ArtistSearchResponse
import com.housecanary.main.itunesmusicapp.network.models.SongResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface ItunesService {

    @GET("search")
    fun searchArtists(@QueryMap params : Map<String, String>) : Single<ArtistSearchResponse>

    @GET("lookup")
    fun lookupAlbums(@QueryMap params: Map<String, String>) : Single<AlbumResponse>

    @GET("lookup")
    fun lookupSongs(@QueryMap params: Map<String, String>) : Single<SongResponse>

}