package com.housecanary.main.itunesmusicapp.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup

object AdapterDelegateFactory {

    fun <TItem : DiffItem, TItemView> create(itemClass: Class<TItem>, itemView: Class<TItemView>): AbstractAdapterDelegate<TItem, DiffItem, TItemView> where TItemView : View, TItemView : BindableView<TItem> {
        return object : AbstractAdapterDelegate<TItem, DiffItem, TItemView>() {
            override fun isForViewType(item: DiffItem, items: List<DiffItem>,
                                       position: Int): Boolean {
                return itemClass.isInstance(item)
            }

            override fun onCreateView(parent: ViewGroup): TItemView {
                try {
                    val constructor = itemView.getConstructor(Context::class.java)
                    return constructor.newInstance(parent.context) as TItemView
                } catch (e: Throwable) {
                    throw java.lang.Error(e)
                }

            }

            override fun onBindView(item: TItem, view: TItemView) {
                view.bind(item)
            }
        }
    }

}