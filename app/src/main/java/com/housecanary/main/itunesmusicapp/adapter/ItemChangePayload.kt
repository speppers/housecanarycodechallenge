package com.housecanary.main.itunesmusicapp.adapter

class ItemChangePayload(val oldItems: List<DiffItem>, val newItems: List<DiffItem>)