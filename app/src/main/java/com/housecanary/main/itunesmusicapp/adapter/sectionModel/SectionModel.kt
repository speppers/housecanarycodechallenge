package com.housecanary.main.itunesmusicapp.adapter.sectionModel

import com.housecanary.main.itunesmusicapp.adapter.DiffItem

abstract class SectionModel {

    val items: MutableList<DiffItem> = ArrayList()
    var headerModel: DiffItem? = null

    abstract fun isForSection(item: DiffItem) : Boolean

    fun getItemsWithHeader(): List<DiffItem> {
        val itemsWithHeader = ArrayList<DiffItem>()

        if (hasHeader()) {
            itemsWithHeader.add(headerModel!!)
        }

        itemsWithHeader.addAll(items)

        return itemsWithHeader
    }

    fun hasHeader(): Boolean {
        return headerModel != null
    }
}