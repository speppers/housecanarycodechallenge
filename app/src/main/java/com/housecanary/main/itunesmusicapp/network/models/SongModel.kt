package com.housecanary.main.itunesmusicapp.network.models

class SongModel {
    val wrapperType: String? = null
    val kind: String? = null
    val artistId: Int? = 0
    val collectionId: Int? = 0
    val trackId: Int? = 0
    val artistName: String? = null
    val collectionName: String? = null
    val trackName: String? = null
    val collectionCensoredName: String? = null
    val trackCensoredName: String? = null
    val collectionArtistId: Int? = 0
    val collectionArtistName: String? = null
    val collectionArtistViewUrl: String? = null
    val artistViewUrl: String? = null
    val collectionViewUrl: String? = null
    val trackViewUrl: String? = null
    val previewUrl: String? = null
    val artworkUrl30: String? = null
    val artworkUrl60: String? = null
    val artworkUrl100: String? = null
    val collectionPrice: Double? = 0.0
    val trackPrice: Double? = 0.0
    val releaseDate: String? = null
    val collectionExplicitness: String? = null
    val trackExplicitness: String? = null
    val discCount: Int? = 0
    val discNumber: Int? = 0
    val trackCount: Int? = 0
    val trackNumber: Int? = 0
    val trackTimeMillis: Int? = 0
    val country: String? = null
    val currency: String? = null
    val primaryGenreName: String? = null
    val isStreamable: Boolean? = false
}