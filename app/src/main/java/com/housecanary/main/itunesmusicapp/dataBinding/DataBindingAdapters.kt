package com.housecanary.main.itunesmusicapp.dataBinding

import androidx.databinding.BindingAdapter
import android.text.TextUtils
import android.widget.ImageView
import com.bumptech.glide.Glide

class DataBindingAdapters {
    companion object {
        @JvmStatic
        @BindingAdapter("android:src")
        fun setImageSrc(imageView: ImageView, url: String?) {
            if (url == null || url.isEmpty()) {
                imageView.setImageResource(0)
                return
            }

            Glide.with(imageView.context)
                    .load(url)
                    .into(imageView)
        }
    }
}