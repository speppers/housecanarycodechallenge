package com.housecanary.main.itunesmusicapp.artistProfile.profileHeader

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.housecanary.main.itunesmusicapp.adapter.DiffItem
import com.housecanary.main.itunesmusicapp.network.models.ArtistModel

class ProfileHeaderViewModel(val model: ArtistModel) : BaseObservable(), DiffItem {

    @get:Bindable
    val name: String? by lazy {
        model.artistName
    }

    @get:Bindable
    val type: String? by lazy {
        model.artistType
    }

    @get:Bindable
    val genre: String? by lazy {
        model.primaryGenreName
    }

    @get:Bindable
    val linkUrl: String? by lazy {
        model.artistLinkUrl
    }

    override fun areItemsTheSame(item: DiffItem): Boolean {
        return if (item is ProfileHeaderViewModel) {
            item.model.artistId == model.artistId
        } else {
            false
        }
    }

    override fun areContentsTheSame(item: DiffItem): Boolean {
        val viewModel = item as ProfileHeaderViewModel
        return viewModel.model.artistName == model.artistName &&
                viewModel.model.artistType == model.artistType &&
                viewModel.model.primaryGenreName == model.primaryGenreName &&
                viewModel.model.artistLinkUrl == model.artistLinkUrl
    }

}