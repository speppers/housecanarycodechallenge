package com.housecanary.main.itunesmusicapp.network.models

class SearchResponse(
        val resultCount: Int,
        val results: List<Result>
)