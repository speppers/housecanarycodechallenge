package com.housecanary.main.itunesmusicapp.artistProfile.profileHeader

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.FrameLayout
import com.housecanary.main.itunesmusicapp.adapter.BindableView
import com.housecanary.main.itunesmusicapp.databinding.ViewProfileHeaderBinding

class ProfileHeaderView(context: Context) : FrameLayout(context), BindableView<ProfileHeaderViewModel> {

    private val binding = ViewProfileHeaderBinding.inflate(LayoutInflater.from(context), this, true)

    init {
        layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
    }

    override fun bind(viewModel: ProfileHeaderViewModel) {
        binding.viewModel = viewModel
    }

}