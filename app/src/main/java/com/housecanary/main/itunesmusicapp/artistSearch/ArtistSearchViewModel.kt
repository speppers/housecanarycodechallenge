package com.housecanary.main.itunesmusicapp.artistSearch

import android.app.Application
import com.housecanary.main.itunesmusicapp.adapter.AdapterDelegateFactory
import com.housecanary.main.itunesmusicapp.adapter.DiffAdapter
import com.housecanary.main.itunesmusicapp.artistSearch.artist.ArtistView
import com.housecanary.main.itunesmusicapp.artistSearch.artist.ArtistViewModel
import com.housecanary.main.itunesmusicapp.artistSearch.search.SearchViewModel
import com.housecanary.main.itunesmusicapp.dagger.Injector
import com.housecanary.main.itunesmusicapp.network.repositories.ArtistRepository
import com.housecanary.main.itunesmusicapp.viewModel.ViewModelBase
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.collections.ArrayList

class ArtistSearchViewModel(application: Application) : ViewModelBase(application) {

    @Inject lateinit var artistRepository: ArtistRepository
    var searchViewModel: SearchViewModel = SearchViewModel()
    val adapter: DiffAdapter = DiffAdapter(listOf(AdapterDelegateFactory.create(ArtistViewModel::class.java, ArtistView::class.java)))

    init {
        Injector.component.inject(this)

        addDisposable(searchViewModel
                .searchObservable
                .debounce(500, TimeUnit.MILLISECONDS)
                .filter { it.length > 2 }
                .flatMap { return@flatMap artistRepository.get(it).toObservable() }
                .subscribe({ models ->
                    adapter.update(models.map { ArtistViewModel(it) })
                }, Throwable::printStackTrace))
    }

    fun clearItems() {
        adapter.update(ArrayList())
    }

}