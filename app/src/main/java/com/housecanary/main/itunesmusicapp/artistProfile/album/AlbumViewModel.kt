package com.housecanary.main.itunesmusicapp.artistProfile.album

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.housecanary.main.itunesmusicapp.adapter.DiffItem
import com.housecanary.main.itunesmusicapp.network.models.AlbumModel

class AlbumViewModel(val model: AlbumModel) : BaseObservable(), DiffItem {

    @get:Bindable
    val name: String? by lazy {
        model.collectionName
    }

    @get:Bindable
    val imageUrl: String? by lazy {
        model.artworkUrl100 ?: model.artworkUrl60
    }

    override fun areItemsTheSame(item: DiffItem): Boolean {
        return if (item is AlbumViewModel) {
            item.model.collectionId == model.collectionId
        } else {
            false
        }
    }

    override fun areContentsTheSame(item: DiffItem): Boolean {
        val viewModel = item as AlbumViewModel
        return viewModel.model.collectionName == model.collectionName &&
                viewModel.model.artworkUrl100 == model.artworkUrl100 &&
                viewModel.model.artworkUrl60 == model.artworkUrl60
    }

}