package com.housecanary.main.itunesmusicapp.network.models

class SongResponse(
        val resultCount: Int,
        val results: List<SongModel>
)