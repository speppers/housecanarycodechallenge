package com.housecanary.main.itunesmusicapp.adapter

interface DiffItem {

    fun areItemsTheSame(item: DiffItem): Boolean
    fun areContentsTheSame(item: DiffItem): Boolean

}