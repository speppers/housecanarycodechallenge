package com.housecanary.main.itunesmusicapp.album.song

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.housecanary.main.itunesmusicapp.adapter.DiffItem
import com.housecanary.main.itunesmusicapp.network.models.SongModel

class SongViewModel(val model: SongModel) : BaseObservable(), DiffItem {

    @get:Bindable
    val name: String? by lazy {
        model.trackName
    }

    override fun areItemsTheSame(item: DiffItem): Boolean {
        return if (item is SongViewModel) {
            item.model.trackId == model.trackId
        } else {
            false
        }
    }

    override fun areContentsTheSame(item: DiffItem): Boolean {
        val viewModel = item as SongViewModel
        return viewModel.model.trackName == model.trackName
    }

}