package com.housecanary.main.itunesmusicapp.adapter

import androidx.recyclerview.widget.DiffUtil
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import com.housecanary.main.itunesmusicapp.adapter.sectionModel.DefaultSectionModel
import com.housecanary.main.itunesmusicapp.adapter.sectionModel.SectionModel

class DiffAdapter(private val delegates: List<AdapterDelegate<MutableList<DiffItem>>>) : ListDelegationAdapter<MutableList<DiffItem>>() {

    private val sectionModels: MutableList<SectionModel> = ArrayList()

    init {
        items = ArrayList()
        setDelegates()
        sectionModels.add(DefaultSectionModel())
    }

    fun setSections(sections: List<SectionModel>) {
        sectionModels.clear()
        sectionModels.addAll(sections)
    }

    private fun setDelegates() {
        for (delegate in delegates) {
            delegatesManager.addDelegate(delegate)
        }
    }

    fun update(newItems: List<DiffItem>) {
        updateSections(newItems)

        val sectionItems = getItemsFromSections()
        val payload = ItemChangePayload(ArrayList(items), sectionItems)
        val diffResult = DiffUtil.calculateDiff(AdapterDiffCallback(payload))

        items.clear()
        items.addAll(sectionItems)

        diffResult.dispatchUpdatesTo(this)
    }

    private fun updateSections(newItems: List<DiffItem>) {
        for (section in sectionModels) {
            section.items.clear()
            for (item in newItems) {
                if (section.isForSection(item)) {
                    section.items.add(item)
                }
            }
        }
    }

    private fun getItemsFromSections() : MutableList<DiffItem> {
        val items: MutableList<DiffItem> = ArrayList()

        for (section in sectionModels) {
            items.addAll(section.getItemsWithHeader())
        }

        return items
    }
}