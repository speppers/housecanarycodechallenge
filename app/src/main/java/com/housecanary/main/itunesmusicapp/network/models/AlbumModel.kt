package com.housecanary.main.itunesmusicapp.network.models

import org.parceler.Parcel

@Parcel
class AlbumModel {
    val wrapperType: String? = null
    val collectionType: String? = null
    val artistId: Int? = 0
    var collectionId: Int = 0
    val amgArtistId: Int? = 0
    val artistName: String? = null
    val collectionName: String? = null
    val collectionCensoredName: String? = null
    val artistViewUrl: String? = null
    val collectionViewUrl: String? = null
    val artworkUrl60: String? = null
    val artworkUrl100: String? = null
    val collectionPrice: Double? = 0.0
    val collectionExplicitness: String? = null
    val trackCount: Int? = 0
    val copyright: String? = null
    val country: String? = null
    val currency: String? = null
    val releaseDate: String? = null
    val primaryGenreName: String? = null
}