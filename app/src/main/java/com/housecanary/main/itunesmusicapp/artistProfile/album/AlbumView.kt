package com.housecanary.main.itunesmusicapp.artistProfile.album

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import com.housecanary.main.itunesmusicapp.Constants
import com.housecanary.main.itunesmusicapp.adapter.BindableView
import com.housecanary.main.itunesmusicapp.album.AlbumDisplayActivity
import com.housecanary.main.itunesmusicapp.databinding.ViewAlbumBinding
import org.parceler.Parcels

class AlbumView(context: Context) : FrameLayout(context), BindableView<AlbumViewModel> {

    private val binding = ViewAlbumBinding.inflate(LayoutInflater.from(context), this, true)
    private var viewModel: AlbumViewModel? = null

    init {
        layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)

        setOnClickListener {
            val intent = Intent(context, AlbumDisplayActivity::class.java).apply {
                putExtra(Constants.album, Parcels.wrap(viewModel?.model))
            }
            context.startActivity(intent)
        }
    }

    override fun bind(viewModel: AlbumViewModel) {
        this.viewModel = viewModel
        binding.viewModel = viewModel
    }

}