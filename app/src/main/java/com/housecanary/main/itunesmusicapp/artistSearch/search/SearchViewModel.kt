package com.housecanary.main.itunesmusicapp.artistSearch.search

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class SearchViewModel : BaseObservable() {

    private val searchSubject: PublishSubject<String> = PublishSubject.create()
    private var searchText: String = ""

    val searchObservable: Observable<String> by lazy {
        searchSubject
    }

    fun clearSearchText() {
        push("")
    }

    fun push(searchText: String) {
        setSearchText(searchText)
        searchSubject.onNext(searchText)
    }

    @Bindable
    fun getSearchText(): String {
        return searchText
    }

    private fun setSearchText(searchText: String) {
        this.searchText = searchText
    }

}